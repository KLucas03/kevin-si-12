#include <SDI12.h>

#define DATAPIN 6         // change to the proper pin
SDI12 mySDI12(DATAPIN);

const char* sdiResponse = "";
const char* myCommand = "";

void setup() {
  USB.ON();

  mySDI12.beging();
  //PWR.setSensorPower(SENS_5V, SENS_ON);
  //Serial.begin(115200);
}

void loop() {


//first command to take a measurement
  myCommand = "?!";             // change "0" to the correct address of the sensor
       // echo command to terminal

  mySDI12.sendCommand1(myCommand);
  delay(30);                     // wait a while for a response


  while (!mySDI12.available1()) {  // build response string
    USB.println("is available");
    char c = mySDI12.read1();
    if ((c != '\n') && (c != '\r')) 
    {
      sdiResponse += c;
      delay(5);
    }
    USB.println("not available");
  }
  if (sizeof(sdiResponse) > 1)
  {
    USB.println(sdiResponse);
  }
 //write the response to the screen
  mySDI12.flush1();


  delay(99999);


                  // delay between taking reading and requesting data
}
