#ifndef SDI12_h
#define SDI12_h

                //  Import Required Libraries
//#include <avr/interrupt.h>      // interrupt handling
//#include <parity.h>         // optimized parity bit handling
#include <inttypes.h>     // integer types library
//#include <Arduino.h>            // Arduino core library
#include <Waspmote.h>
//#include <Stream.h>       // Arduino Stream library

class SDI12
{
protected:
  int peekNextDigit();      // override of Stream equivalent to allow custom TIMEOUT
private:
  static SDI12 *_activeObject;  // static pointer to active SDI12 instance
  void settheState(uint8_t state); // sets the state of the SDI12 objects
  void wakeSensors();     // used to wake up the SDI12 bus
  void writeChar(uint8_t out);  // used to send a char out on the data line
  void receiveChar();     // used by the ISR to grab a char from data line
  
public:
  int TIMEOUT;
  SDI12(uint8_t dataPin);   // constructor
  ~SDI12();           // destructor
  void beging();         // enable SDI-12 object
  void ending();         // disable SDI-12 object
  
  void forceHold();       // sets line state to HOLDING
  void sendCommand1(const char* cmd); // sends the String cmd out on the data line
    
  int available1();      // returns the number of bytes available in buffer
  int peek1();       // reveals next byte in buffer without consuming
  int read1();       // returns next byte in the buffer (consumes)
  void flush1();       // clears the buffer 
  virtual size_t write(uint8_t byte){}; // dummy function required to inherit from Stream

  bool setActive();     // set this instance as the active SDI-12 instance
  bool isActive();      // check if this instance is active

  static inline void handleInterrupt(); // intermediary used by the ISR
};

#endif