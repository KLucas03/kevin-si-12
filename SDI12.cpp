#include <SDI12.h>            // 0.1 header file for this library

#define _BUFFER_SIZE 64         // 0.2 max RX buffer size   
#define SDIDISABLED 0            // 0.3 value for SDIDISABLED theState
#define SDIENABLED 1           // 0.4 value for SDIENABLED theState
#define SDIHOLDING 2             // 0.5 value for SDIDISABLED theState
#define SDITRANSMITTING 3          // 0.6 value for SDITRANSMITTING theState
#define SDILISTENING 4           // 0.7 value for SDILISTENING theState
#define SPACING 830           // 0.8 bit timing in microseconds
int TIMEOUT = -9999;          // 0.9 value to return to indicate TIMEOUT

#define parity_even_bit(val)        \
(__extension__({          \
  unsigned char __t;        \
  __asm__ (         \
    "mov __tmp_reg__,%0" "\n\t"   \
    "swap %0" "\n\t"      \
    "eor %0,__tmp_reg__" "\n\t"   \
    "mov __tmp_reg__,%0" "\n\t"   \
    "lsr %0" "\n\t"       \
    "lsr %0" "\n\t"       \
    "eor %0,__tmp_reg__"      \
    : "=r" (__t)        \
    : "0" ((unsigned char)(val))    \
    : "r0"          \
  );            \
  (((__t + 1) >> 1) & 1);       \
 }))

SDI12 *SDI12::_activeObject = NULL;   // 0.10 pointer to active SDI12 object
uint8_t _dataPin;             // 0.11 reference to the data pin
bool _bufferOverflow;         // 0.12 buffer overflow status


// See section 0 above.     // 1.1 - max buffer size
char _rxBuffer[_BUFFER_SIZE];   // 1.2 - buff for incoming
uint8_t _rxBufferHead = 0;    // 1.3 - index of buff head
uint8_t _rxBufferTail = 0;    // 1.4 - index of buff tail

// 2.1 - sets the theState of the SDI-12 object. 
void SDI12::settheState(uint8_t theState)
{
  if(theState == SDIHOLDING)
  {
	USB.println("HOLDING");
    pinMode(_dataPin,OUTPUT);
    digitalWrite(_dataPin,LOW);
	disableInterrupts(SENS_INT);
   
  }
  if(theState == SDITRANSMITTING)
  {
	USB.println("Transmitting");
    pinMode(_dataPin,OUTPUT);
    //cli();      // supplied by Arduino.h, same as cli()
	delay(10);
	noInterrupts(); 
  }
  if(theState == SDILISTENING)
  {
	USB.println("Listening");
    digitalWrite(_dataPin,LOW);
    pinMode(_dataPin,INPUT); 
    //sei(); //BROKEN+++++++++++++++++++++++++++++++++++++++++
	delay(10);
	interrupts();
	sbi(UCSR1B, RXEN1);
	sbi(UCSR1B, TXEN1);
    enableInterrupts(SENS_INT);
    //*digitalPinToPCICR(_dataPin) |= (1<<digitalPinToPCICRbit(_dataPin));  //enable global interrupts
	
  } 
  else 
  {   // implies theState==SDIDISABLED 
    USB.println("Disabled");
    digitalWrite(_dataPin,LOW); 
    pinMode(_dataPin,INPUT);
   disableInterrupts(SENS_INT);
   // if(!*digitalPinToPCMSK(_dataPin)){
   //   *digitalPinToPCICR(_dataPin) &= ~(1<<digitalPinToPCICRbit(_dataPin));
    }
  }

// 2.2 - forces a SDIHOLDING theState. 
void SDI12::forceHold(){
  settheState(SDIHOLDING); 
}


//  3.1 Constructor
SDI12::SDI12(uint8_t dataPin){ _bufferOverflow = false; _dataPin = dataPin; }   

//  3.2 Destructor
SDI12::~SDI12(){ settheState(SDIDISABLED); USB.println("CRASH");}

//  3.3 Begin
void SDI12::beging() { settheState(SDIHOLDING); setActive(); }

//  3.4 End
void SDI12::ending() { settheState(SDIDISABLED); }




// 4.1 - this function wakes up the entire sensor bus
void SDI12::wakeSensors(){
  USB.println("wakeSensors() started");
  settheState(SDITRANSMITTING); //+++++++=BROKEN++++++++++++++++
  digitalWrite(_dataPin, HIGH); 
  delayMicroseconds(12100); 
  digitalWrite(_dataPin, LOW); 
  delayMicroseconds(8400);
}

// 4.2 - this function writes a character out on the data line
void SDI12::writeChar(uint8_t out)
{
	USB.println("writechar()");
  out |= (parity_even_bit(out)<<7);     // 4.2.1 - parity bit

  digitalWrite(_dataPin, HIGH);       // 4.2.2 - start bit
  delayMicroseconds(SPACING);

  for (byte bitMask = 0x01; bitMask; bitMask<<=1){ // 4.2.3 - send payload
    if(out & bitMask){
      digitalWrite(_dataPin, LOW);
    }
    else{
      digitalWrite(_dataPin, HIGH);
    } 
    delayMicroseconds(SPACING);
  }
  
  digitalWrite(_dataPin, LOW);        // 4.2.4 - stop bit
  delayMicroseconds(SPACING); 
}

//  4.3 - this function sends out the characters of the String cmd, one by one
void SDI12::sendCommand1(const char* cmd){
  USB.println("sendcommand() started");
  wakeSensors();              // wake up sensors
  
  
  int i =0;
  while(i<strlen(cmd))
  {
	USB.println(cmd[i]);
    writeChar(cmd[i]);
	i++;
  }
  settheState(SDILISTENING);            // listen for reply
}








// 5.1 - reveals the number of characters available in the buffer
int SDI12::available1()
{
  USB.println("available()");
  if(_bufferOverflow)
  {
	  
	  return -1; 
  }
  return (_rxBufferTail + _BUFFER_SIZE - _rxBufferHead) % _BUFFER_SIZE;
}

// 5.2 - reveals the next character in the buffer without consuming
int SDI12::peek1()
{
  if (_rxBufferHead == _rxBufferTail) return -1;   // Empty buffer? If yes, -1
  return _rxBuffer[_rxBufferHead];        // Otherwise, read from "head"
}

// 5.3 - a public function that clears the buffer contents and
// resets the status of the buffer overflow. 
void SDI12::flush1()
{
	USB.println("flush");
  _rxBufferHead = _rxBufferTail = 0;
  _bufferOverflow = false; 
}

// 5.4 - reads in the next character from the buffer (and moves the index ahead)
int SDI12::read1()
{
  USB.println("read()");
  _bufferOverflow = false;      //reading makes room in the buffer
  if (_rxBufferHead == _rxBufferTail)
  {
	  USB.println("-1");
	  return -1; 
  }  // Empty buffer? If yes, -1
  
  uint8_t nextChar = _rxBuffer[_rxBufferHead];  // Otherwise, grab char at head
  _rxBufferHead = (_rxBufferHead + 1) % _BUFFER_SIZE;   // increment head
  USB.println(nextChar);
  return nextChar;                    // return the char
}


long _timeout = 9999;
int timedPeek()
{
  int c;
  long _startMillis = millis();
  do {
    //c = peek1();
    if (_rxBufferHead == _rxBufferTail) c = -1;   // Empty buffer? If yes, -1
    c = _rxBuffer[_rxBufferHead];        // Otherwise, read from "head"
    if (c >= 0)return c;
  
  } while(millis() - _startMillis < _timeout);
  return -1;     // -1 indicates timeout
}


// 5.5 - this function is called by the Stream class when parsing digits
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int SDI12::peekNextDigit()
{
  int c;
  while (1) {
    c = timedPeek();
    if (c < 0)
    {
      return TIMEOUT; // timeout
    }
    if (c == '-')
    {
      return c;
    }
    if (c >= '0' && c <= '9')
    {
      return c;
    }
    read1(); // discard non-numeric
  }
}





// 6.1 - a method for setting the current object as the active object
bool SDI12::setActive()
{
   USB.println("setting Active");
  if (_activeObject != this)
  {
    settheState(SDIHOLDING); 
    _activeObject = this;
    return true;
  }
  return false;
}

// 6.2 - a method for checking if this object is the active object
bool SDI12::isActive() { return this == _activeObject; }



// 7.1 - Passes off responsibility for the interrupt to the active object. 
inline void SDI12::handleInterrupt(){
  if (_activeObject) _activeObject->receiveChar();
}

// 7.2 - Quickly reads a new character into the buffer. 
void SDI12::receiveChar()
{
  if (digitalRead(_dataPin))        // 7.2.1 - Start bit?
  {
    uint8_t newChar = 0;          // 7.2.2 - Make room for char.
    
    delayMicroseconds(SPACING/2);     // 7.2.3 - Wait 1/2 SPACING

    for (uint8_t i=0x1; i<0x80; i <<= 1)  // 7.2.4 - read the 7 data bits
    {
      delayMicroseconds(SPACING);
      uint8_t noti = ~i;
      if (!digitalRead(_dataPin))
        newChar |= i;
      else 
        newChar &= noti;
    }
    
    delayMicroseconds(SPACING);       // 7.2.5 - Skip the parity bit. 
  delayMicroseconds(SPACING);       // 7.2.6 - Skip the stop bit. 

                    // 7.2.7 - Overflow? If not, proceed.
    if ((_rxBufferTail + 1) % _BUFFER_SIZE == _rxBufferHead) 
    { _bufferOverflow = true; 
    } else {              // 7.2.8 - Save char, advance tail. 
      _rxBuffer[_rxBufferTail] = newChar; 
      _rxBufferTail = (_rxBufferTail + 1) % _BUFFER_SIZE;
    }
  }
}

//7.3
#if defined(PCINT0_vect)
ISR(PCINT0_vect){ SDI12::handleInterrupt(); }
#endif

#if defined(PCINT1_vect)
ISR(PCINT1_vect){ SDI12::handleInterrupt(); }
#endif

#if defined(PCINT2_vect)
ISR(PCINT2_vect){ SDI12::handleInterrupt(); }
#endif

#if defined(PCINT3_vect)
ISR(PCINT3_vect){ SDI12::handleInterrupt(); }
#endif
